import os
import numpy as np 
import cv2
import clock_angle_overlay
import BoundingBox2ClockAngle
import pydicom
import pandas as pd


def Mark_and_Protract(clock_angle, cmfn, dcm_filename, nipple_filename, png_filename, breast_contour_filename):
    
    im_overlay, image_size = BoundingBox2ClockAngle.ReadDicom(dcm_filename,metadata_filename)
    image_laterality,view_position,pixel_size,nipple,nipple_pointing,clock = BoundingBox2ClockAngle.Read(nipple_filename,image_size,breast_contour_filename)

    mass_color  = (255,255,0)
    calcs_color = (255,0,255)
    font        = cv2.FONT_HERSHEY_SIMPLEX

    lesion = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
    x = lesion.x
    y = lesion.y

    
    #compute and draw protrator 
    clock_preset = np.array(['12:00','11:00','10:00','9:00','8:00','7:00','6:00','5:00','4:00','3:00','2:00','1:00']).flatten()
    cmfn_preset  = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]).flatten()

    #compute lines with preset clock angles
    curves = []
    label_position_x = []
    label_position_y = []
    for clock_angle in clock_preset:
        curve = []
        for cmfn in cmfn_preset:
            lesion      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
            pt          = []
            pt.append(int(round(lesion.x)))
            pt.append(int(round(lesion.y)))
            pt = np.array(pt).flatten()
            curve.append(pt)
        curves.append(curve)
        #render mass score
        x_min      = curves[-1][5][0]
        y_min      = curves[-1][5][1]
        found = np.array(np.where((label_position_x == x_min) & (label_position_y == y_min))).flatten()
        if len(found) == 0:
            im_overlay = cv2.putText(im_overlay,clock_angle,(x_min,y_min-5), font, 1.75,mass_color,2,cv2.LINE_AA)
            label_position_x.append(x_min)
            label_position_y.append(y_min)
        else:
            x_min      = curves[-1][7][0]
            y_min      = curves[-1][7][1]
            im_overlay = cv2.putText(im_overlay,clock_angle,(x_min,y_min-5), font, 1.75,mass_color,2,cv2.LINE_AA)

    curves = np.array(curves)
    im_overlay = cv2.polylines(im_overlay, curves, True, (0,255,255), 1)

    #compute curves with preset cmfn
    curves = []
    for cmfn in cmfn_preset:
        curve = []
        for clock_angle in clock_preset:
            lesion      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
            pt          = []
            pt.append(int(round(lesion.x)))
            pt.append(int(round(lesion.y)))
            pt = np.array(pt).flatten()
            curve.append(pt)
        curves.append(curve)
    curves = np.array(curves)
    im_overlay = cv2.polylines(im_overlay, curves, True, (0,255,255), 1)
    im_overlay = cv2.circle(im_overlay,(int(x),int(y)),5,(0,0,255),-1)
    cv2.imwrite(png_filename,im_overlay)



if __name__ == "__main__":
	a = pd.read_csv('a.csv')
	#print a
	Dir = os.getcwd()
	#print Dir
	print len(a['sop_instance_uid'])
	for i in range(len(a['sop_instance_uid'])):
		sopuid = a.loc[i,'sop_instance_uid']
		dcm_filename    = Dir + '/Priors/' + 'DCMs/' + sopuid + '.dcm'
		png_filename    = Dir + '/Priors/' + 'outputs/' + sopuid +'._1_.png'
		nipple_filename = Dir + '/Priors/' + 'JSON-Outputs/' + sopuid + '_MassCalc.json'
		metadata_filename = Dir + '/Priors/' + 'JSON-Outputs/' + sopuid + '_MetaData.json'
		breast_contour_filename = Dir + '/Priors/' + 'BreastBoundary_Output/' + sopuid + '.json'
		cmfn = a.loc[i,'Distance']
		print cmfn
		hours = a.loc[i,'Hours']
		minutes = a.loc[i,'minutes']        
		clock_angle = str(int(hours)) + ":" + str(int(minutes))
		Mark_and_Protract(clock_angle, cmfn, dcm_filename, nipple_filename, png_filename, breast_contour_filename)