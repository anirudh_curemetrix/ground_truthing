import os 
import subprocess


Current_directory = os.getcwd()
#print Current_directory
'''
Running the CM-PyQ.py
'''
os.chdir('./q-algorithm/')
List4 = 'python ' + 'CM-PyQ.py ' + '-i ' + Current_directory + '/DCMs/ ' + '-o ' + Current_directory + '/ ' + '-g 1 ' + '-sC '
#print List4
p3 = subprocess.Popen(List4, bufsize = -1, shell=True)
p3.wait()

List5 = 'python ' + 'CM-PyQ.py ' + '-i ' + Current_directory + '/Priors/DCMs/ ' + '-o ' + Current_directory + '/Priors/ ' + '-g 1 ' + '-sC '
#print List5
p4 = subprocess.Popen(List5, bufsize = -1, shell=True)
p4.wait()

os.chdir(Current_directory)
print os.getcwd()


'''
Runing the Breast Boundary Detection Code
'''
List6 = 'python ' + 'Breast_Boundary_Detection.py ' + '-location ' + Current_directory + '/'
p3 = subprocess.Popen(List6, bufsize = -1, shell=True)
p3.wait()

List7 = 'python ' + 'Breast_Boundary_Detection.py ' + '-location ' + Current_directory + '/Priors/'
p3 = subprocess.Popen(List7, bufsize = -1, shell=True)
p3.wait()


'''
Running the overlaying protractor code
'''
List8 = 'python ' + 'BoundingBox2ClockAngle.py ' + '-Current ' + Current_directory + '/mammo_rad_gt_screening.csv ' + '-Prior ' + Current_directory + '/db_uf_screening_priors.csv'
p4 = subprocess.Popen(List8, bufsize = -1, shell=True)
p4.wait()

List9 = 'python ' + 'MarkAndProtract.py '
p5 = subprocess.Popen(List9, bufsize = -1, shell = True)
p5.wait()