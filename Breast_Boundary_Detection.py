import os
import numpy as np
import cv2
#import matplotlib.pyplot as plt
import glob 
import json
import argparse

parser = argparse.ArgumentParser(description = 'Creating Jsons for breast boundary')
parser.add_argument('-location', dest = 'location', help = 'location to PNGs_OriginalSize', default = '/mnt/Array/share/users/anirudh/Ground_Truthing/')
args = parser.parse_args()
path = args.location
##########Chaning Directory and reading all the images########
os.chdir(path)
Images = []
Filenames = []
for images in glob.glob('PNGs_OriginalSize/*.png'):
    Image = cv2.imread(images,0).copy()
    if not json.loads(open(images.replace('PNGs_OriginalSize','JSON-Outputs').replace('.png','_MassCalc.json')).read())['orientation_matches_png']:
        Image = cv2.flip(Image,1)
    Images.append(Image)
    Filenames.append(images)
    

################Main Code for finding breast boundary using contour method################
for i in range(len(Images)):
    #Finding Threshold for individual Images
    InputImage = Images[i]
    InputImage = InputImage[np.nonzero(InputImage)]
    threshold = np.min(InputImage)
    ret,thresh = cv2.threshold(Images[i],threshold,255,cv2.THRESH_BINARY) 
    #plt.imshow(thresh, cmap = 'gray')
    #Flood-Fill Algorithm 
    im_floodfill = thresh.copy()
    h, w = thresh.shape[:2]
    mask = np.zeros((h+2, w+2), np.uint8)
    cv2.floodFill(im_floodfill, mask, (0,0), 255);
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)
    floodfill_output = thresh | im_floodfill_inv
    #Finding Contours
    backtorgb = cv2.cvtColor(Images[i],cv2.COLOR_GRAY2RGB)
    contours, hierarchy = cv2.findContours(floodfill_output,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
    #Finding Maximum Length Contour
    length_contours = []
    max_length = 0
    for j in range(len(contours)):
        if (len(contours[j]) > max_length):
            max_length = len(contours[j])
            index = j
    #print index
    img = backtorgb
    cv2.drawContours(img,contours,index, (0,255,0), 3) #cv2: no explicit assignment; cv3 or above: put 'img = '
    cv2.imwrite(Filenames[i].replace('PNGs_OriginalSize','BreastBoundary_Output'),img)
    open(Filenames[i].replace('PNGs_OriginalSize','BreastBoundary_Output').replace('.png','.json'),'w').write(json.dumps({"contour":contours[index].tolist()},indent=1))