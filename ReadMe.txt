Steps to follow when running this script: 
1. Create the required directories by running the command:
"python Create_Directories.py "
2. Place the dicoms read from current.csv in folder DCMs
3. PLace the dicoms read from priors.csv in folder /Priors/DCMS/
4. Uzip the tf-1.7-env and q-algorithm directories by running "python Unzip.py"
5. Activate the environemnt using the following commands: 
"source ./tf-1.7-env/bin/activate"
"export LD_LIBRARY_PATH=/usr/local/cuda-9.0/lib64/"

We need to test the above functionality. If it doesn't work activate in an environment that has tensorflow 1.7 and CUDA 9.0 support 
This might require requirements.txt. Not made yet 

6. Run the command "python RUN_CMPYQ.py"

Note: If you are using a different csv other than the one in this repository, change the lines 40 in RUN_CMPYQ.py with the csv names you want. 
-Current indicates name of the csv that has information on the currents 
-Priors indicates the name o fthe csv that has prior information 

The Output PNGs (protractor overlayed) are stored in outputs and in /Priors/outputs/ 
The PNGs in /Priors/outputs are the once to be given to the intern for ground-truthing 
The code also outputs a csv file named "a.csv" This file has the possible distance, clock angle and hour for the intern to look at while ground-turthing priors

The PNGs are not always correct. This is because there is discripancy in the breast boundary detection for some images which are in different format.