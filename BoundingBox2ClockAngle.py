import argparse
import numpy as np
import cv2
import os 
#import matplotlib.pyplot as plt 
import pandas as pd 
import clock_angle_overlay
import pydicom
#import matplotlib.pyplot as plt
import json
import math

class POINT:
    def __init__(self,x,y):
            self.x = x
            self.y = y

def CenterOfRectangle(Current_csv,i):
    '''
    Finds the Center of the rectangular box
    Input: Dataframe from csv file
    Ouput: x,y co-ordinates of the center of rectangular box in dicom co-ordinates
    '''
    xmin = Current_csv.loc[i,'xmin']
    ymin = Current_csv.loc[i,'ymin']
    xmax = Current_csv.loc[i,'xmax']
    ymax = Current_csv.loc[i,'ymax']
    x = (xmin + xmax)/2
    y = (ymin + ymax)/2
    lesion_radius = np.amin([0.5*(xmax - xmin),0.5*(ymax - ymin)])
    return x,y,lesion_radius


def getbackClockAngle_and_distance(nipple_filename,image_size,x,y):
    '''
    Function to calculate the Clock Angel and distance
    '''
    try: 
        image_laterality,view_position,pixel_size,nipple,nipple_pointing,clock = Read(nipple_filename, image_size, breast_contour_filename)
        x_in = 0
        y_in = 0
        view = view_position
        laterality = image_laterality
        if nipple_pointing == 'r':
            if (view == 'mlo') | (view == 'ml'):
                dx = x - nipple.x
                dy = y - nipple.y
                slope = dy/dx
                cmfn = (((-dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0
                y_in = -(slope*(clock.x - nipple.x) + nipple.y - clock.y)
                #print y_in
            elif view == 'cc':
                if laterality == 'l':
                    dx = x - nipple.x
                    dy = y - nipple.y
                    slope = dy/dx
                    cmfn = (((-dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0
                    x_in = -(slope*(clock.x - nipple.x) + nipple.y - clock.y)
                    #print x_in
                else:
                    dx = x - nipple.x
                    dy = y - nipple.y
                    slope = dy/dx
                    cmfn = (((-dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0
                    x_in = slope*(clock.x - nipple.x) + nipple.y - clock.y
                    #print x_in    
            else:
                print('invalid view: ',view)
        elif nipple_pointing == 'l':
            if (view == 'mlo') | (view == 'ml'):
                dx = x - nipple.x
                dy = y - nipple.y
                slope = dy/dx
                cmfn = (((dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0    
                y_in = -(slope*(clock.x - nipple.x) + nipple.y - clock.y)
                #print y_in
            elif view == 'cc':
                if laterality == 'l':
                    dx = x - nipple.x
                    dy = y - nipple.y
                    slope = dy/dx
                    cmfn = (((dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0  
                    x_in = -(slope*(clock.x - nipple.x) + nipple.y - clock.y)
                    #print x_in
                else: 
                    dx = x - nipple.x
                    dy = y - nipple.y
                    slope = dy/dx
                    cmfn = (((dx)*np.sqrt(1 + slope * slope))*(pixel_size))/10.0
                    x_in = slope*(clock.x - nipple.x) + nipple.y - clock.y
                    #print x_in
            else:
                print('invalid view: ',view)

        if x_in == 0:
            variable = float(y_in)/float(clock.r)
            if variable > 1.0:
                variable = 1.0
                angle = np.arcsin(variable)
                #print (180/np.pi)*angle
                if angle > 0:
                    angle = 0.5* np.pi - angle
                elif angle < 0: 
                    angle = 0.5* np.pi + abs(angle)
                else:
                    angle = angle
                hours = (6/np.pi)*angle
                minutes = (hours - math.trunc(hours))*60
            elif variable < -1.0:
                variable = -1.0
                angle = np.arcsin(variable)
                #print (180/np.pi)*angle
                if angle > 0:
                    angle = 0.5* np.pi - angle
                elif angle < 0: 
                    angle = 0.5* np.pi + abs(angle)
                else:
                    angle = angle
                hours = (6/np.pi)*angle
                minutes = (hours - math.trunc(hours))*60
            else:
                angle = np.arcsin(variable)
                #print (180/np.pi)*angle
                if angle > 0:
                    angle = 0.5* np.pi - angle
                elif angle < 0: 
                    angle = 0.5* np.pi + abs(angle)
                else:
                    angle = angle
                hours = (6/np.pi)*angle
                minutes = (hours - math.trunc(hours))*60


        elif y_in == 0:
            variable = float(x_in)/float(clock.r)
            if variable > 1.0:
                varaible = 1.0
                angle = np.arccos(variable)
                #print (180/np.pi)*angle
                hours = (6/np.pi)*angle + 3
                minutes = (hours - math.trunc(hours))*60
            elif variable < -1:
                variable = -1.0
                angle = np.arccos(variable)
                #print (180/np.pi)*angle
                hours = (6/np.pi)*angle + 3
                minutes = (hours - math.trunc(hours))*60
            else:
                angle = np.arccos(variable)
                #print (180/np.pi)*angle
                hours = (6/np.pi)*angle + 3
                minutes = (hours - math.trunc(hours))*60
        else:
            pass

        tmp = 0   
        if (view == 'mlo') | (view == 'ml'):
            hours2 = 12 - int(hours)
            minutes2 = minutes
        if view == 'cc':
            if hours == 6:
                hours2 = 12
            elif hours > 6:
                tmp = int(hours) - 6
                hours2 = 12 - tmp
            else: 
                tmp = int(hours) - 3
                print(tmp)
                hours2 = 3 - tmp
                print(hours2)
            minutes2 = minutes
        #print "Distance :%f"%cmfn
        #print "Hours :%d Minutes :%.2f"%(hours,minutes)
        #print "Hours2 :%d Minutes2 :%.2f"%(hours2,minutes2)
        minutesrounded = 0
        if int(minutes) < 15:
            minutesrounded = 0
        elif int(minutes) > 15:
            minutesrounded = 30
        elif int(minutes) > 30 and int(minutes) < 45:
            minutesrounded = 30
        elif int(minutes) > 45:
            hours = hours + 1
            minutes = 0
    except ValueError : 
        '''
        Yet to write a fix for this 
        This error is caused if the Ground-truth bounding box lies outside the protractor region
        '''
        #print "The x,y co-ordinates lie outside the proctractor"
        #continue_flag = 1 
        #return None

    return cmfn, int(hours), minutesrounded
    
def ReadDicom(dcm_filename,metadata_filename):
    if os.path.isfile(dcm_filename):
        #read DCM
        print 'reading: ',dcm_filename
        dicom_object = pydicom.dcmread(dcm_filename)
        im = np.array((float(255)/float(4096))* np.array(dicom_object.pixel_array[:,:]).astype('float')).astype('uint8')
        #get image size
        image_size = im.shape
        n_row      = image_size[0]
        n_col      = image_size[1]
        im_resized = cv2.resize(im, (n_col,n_row))
        #store image in im_overlay (3 channels image overlay array)
        im_overlay = np.zeros((n_row,n_col,3)).astype('uint8')
        im_overlay[:,:,0] = np.copy(im_resized)
        im_overlay[:,:,1] = np.copy(im_resized)
        im_overlay[:,:,2] = np.copy(im_resized)

        #with open(metadata_filename, 'r') as f:
        #    data = json.load(f)
        #if data['flip'] == True:
        #    im_overlay = cv2.flip(im_overlay,1)

    return im_overlay, image_size

def Read(nipple_filename, image_size, breast_contour_filename):
    #im_overlay, image_size = ReadDicom(dcm_filename,metadata_filename)
    if os.path.isfile(nipple_filename):
        image_laterality,view_position,pixel_size,xmin,ymin,xmax,ymax,nipple = clock_angle_overlay.get_nipple(nipple_filename)
    if os.path.isfile(breast_contour_filename):
        breast_centroid,breast_contour,contour_x,contour_y = clock_angle_overlay.get_breast_outline(breast_contour_filename)

    #is nipple pointing left or right?
    nipple_pointing = clock_angle_overlay.get_nipple_pointing(nipple,breast_centroid)

    #get clock center and radius
    clock = clock_angle_overlay.get_clock(image_size,contour_x,contour_y,nipple,nipple_pointing)

    #overlay clock on chest wall
    contour_y_min = np.amin(contour_y) #These are used for drawing the clock wall on the chest 
    contour_y_max = np.amax(contour_y)
    #im_overlay = cv2.rectangle(im_overlay, (int(xmin),int(ymin)), (int(xmin),int(ymin)), (0,255,0),2)
    #print nipple_pointing
    return image_laterality,view_position,pixel_size,nipple,nipple_pointing,clock

def DrawProtractor(dcm_filename,nipple_filename,png_filename):
    im_overlay, image_size = ReadDicom(dcm_filename,metadata_filename)
    image_laterality,view_position,pixel_size,nipple,nipple_pointing,clock = Read(nipple_filename,image_size, breast_contour_filename)

    mass_color  = (255,255,0)
    calcs_color = (255,0,255)
    font        = cv2.FONT_HERSHEY_SIMPLEX

    #im_overlay = cv2.circle(im_overlay,(int(x),int(y)),5,(0,0,255),-1)
    #compute and draw protrator 
    clock_preset = np.array(['12:00','11:00','10:00','9:00','8:00','7:00','6:00','5:00','4:00','3:00','2:00','1:00']).flatten()
    cmfn_preset  = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]).flatten()

    #compute lines with preset clock angles
    curves = []
    label_position_x = []
    label_position_y = []
    for clock_angle in clock_preset:
        curve = []
        for cmfn in cmfn_preset:
            lesion      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
            pt          = []
            pt.append(int(round(lesion.x)))
            pt.append(int(round(lesion.y)))
            pt = np.array(pt).flatten()
            curve.append(pt)
        curves.append(curve)
        #render mass score
        x_min      = curves[-1][5][0]
        y_min      = curves[-1][5][1]
        found = np.array(np.where((label_position_x == x_min) & (label_position_y == y_min))).flatten()
        if len(found) == 0:
            im_overlay = cv2.putText(im_overlay,clock_angle,(x_min,y_min-5), font, 1.75,mass_color,2,cv2.LINE_AA)
            label_position_x.append(x_min)
            label_position_y.append(y_min)
        else:
            x_min      = curves[-1][7][0]
            y_min      = curves[-1][7][1]
            im_overlay = cv2.putText(im_overlay,clock_angle,(x_min,y_min-5), font, 1.75,mass_color,2,cv2.LINE_AA)

    curves = np.array(curves)
    im_overlay = cv2.polylines(im_overlay, curves, True, (0,255,255), 1)

    #compute curves with preset cmfn
    curves = []
    for cmfn in cmfn_preset:
        curve = []
        for clock_angle in clock_preset:
            lesion      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
            pt          = []
            pt.append(int(round(lesion.x)))
            pt.append(int(round(lesion.y)))
            pt = np.array(pt).flatten()
            curve.append(pt)
        curves.append(curve)
    curves = np.array(curves)
    im_overlay = cv2.polylines(im_overlay, curves, True, (0,255,255), 1)
    cv2.imwrite(png_filename,im_overlay)

def Mark_and_Protract(clock_angle, dcm_filename, nipple_filename, png_filename):
    lesion = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,laterality,view,nipple_pointing,pixel_spacing)
    x = lesion.x
    y = lesion.y
    im_overlay, image_size = ReadDicom(dcm_filename,metadata_filename)
    image_laterality,view_position,pixel_size,nipple,nipple_pointing,clock = Read(nipple_filename,image_size,breast_contour_filename)

    mass_color  = (255,255,0)
    calcs_color = (255,0,255)
    font        = cv2.FONT_HERSHEY_SIMPLEX

    im_overlay = cv2.circle(im_overlay,(int(x),int(y)),5,(0,0,255),-1)
    #compute and draw protrator 
    clock_preset = np.array(['12:00','11:00','10:00','9:00','8:00','7:00','6:00','5:00','4:00','3:00','2:00','1:00']).flatten()
    cmfn_preset  = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]).flatten()

    #compute lines with preset clock angles
    curves = []
    label_position_x = []
    label_position_y = []
    for clock_angle in clock_preset:
        curve = []
        for cmfn in cmfn_preset:
            lesion      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
            pt          = []
            pt.append(int(round(lesion.x)))
            pt.append(int(round(lesion.y)))
            pt = np.array(pt).flatten()
            curve.append(pt)
        curves.append(curve)
        #render mass score
        x_min      = curves[-1][5][0]
        y_min      = curves[-1][5][1]
        found = np.array(np.where((label_position_x == x_min) & (label_position_y == y_min))).flatten()
        if len(found) == 0:
            im_overlay = cv2.putText(im_overlay,clock_angle,(x_min,y_min-5), font, 1.75,mass_color,2,cv2.LINE_AA)
            label_position_x.append(x_min)
            label_position_y.append(y_min)
        else:
            x_min      = curves[-1][7][0]
            y_min      = curves[-1][7][1]
            im_overlay = cv2.putText(im_overlay,clock_angle,(x_min,y_min-5), font, 1.75,mass_color,2,cv2.LINE_AA)

    curves = np.array(curves)
    im_overlay = cv2.polylines(im_overlay, curves, True, (0,255,255), 1)

    #compute curves with preset cmfn
    curves = []
    for cmfn in cmfn_preset:
        curve = []
        for clock_angle in clock_preset:
            lesion      = clock_angle_overlay.get_coordinates(clock_angle,cmfn,clock,nipple,image_laterality,view_position,nipple_pointing,pixel_size)
            pt          = []
            pt.append(int(round(lesion.x)))
            pt.append(int(round(lesion.y)))
            pt = np.array(pt).flatten()
            curve.append(pt)
        curves.append(curve)
    curves = np.array(curves)
    im_overlay = cv2.polylines(im_overlay, curves, True, (0,255,255), 1)
    cv2.imwrite(png_filename,im_overlay)


if __name__ == "__main__":
    #Reading the csv file
    parser = argparse.ArgumentParser(description = 'Identify Clock Angle and Distance from existing Ground truth')
    parser.add_argument('-Current', dest = 'Current', help = 'csv of Current Information', default = './gt_pr')
    parser.add_argument('-Prior', dest = 'Prior', help = 'csv of Prior Information', default = './gt_pr')
    args = parser.parse_args()
    Current_csv = pd.read_csv(args.Current)
    Prior_csv = pd.read_csv(args.Prior, sep='\s+')
    Dir = os.path.dirname(args.Current)
    
    #print Prior_csv
    headers = list(Prior_csv.columns.values)
    #print headers
    
    for i in range(len(Current_csv['sop_instance_uid'])):
            sopuid = Current_csv.loc[i,'sop_instance_uid']
            dcm_filename    = Dir + '/DCMs/' + sopuid + '.dcm'
            png_filename    = Dir + '/outputs/' + sopuid +'._1_.png'
            nipple_filename = Dir + '/JSON-Outputs/' + sopuid + '_MassCalc.json'
            metadata_filename = Dir + '/JSON-Outputs/' + sopuid + '_MetaData.json'
            breast_contour_filename = Dir + '/BreastBoundary_Output/' + sopuid + '.json'
            image_laterality = Current_csv.loc[i,'image_laterality']
            view_position = Current_csv.loc[i,'view_position']
            patient_id  = Current_csv.loc[i,'patient_name']
            x,y, lesion_radius = CenterOfRectangle(Current_csv,i)
            #print x,y
            image, image_size = ReadDicom(dcm_filename,metadata_filename)
            #print image_size
            DrawProtractor(dcm_filename,nipple_filename,png_filename)
            Distance,hours, minutes = getbackClockAngle_and_distance(nipple_filename,image_size,x,y)
            #Prior_csv = Prior_csv.convert_objects(convert_numeric=True)
            index = Prior_csv.index[Prior_csv['patient_name'] == patient_id].tolist()
            #print index
            for j in range(len(index)):
                #print Prior_csv.loc[index[j],'image_laterality']
                #print Prior_csv.loc[index[j],'view_position']
                #print Prior_csv.loc[index[j],'patient_name']
                if (Prior_csv.loc[index[j],'image_laterality'] == image_laterality) and (Prior_csv.loc[index[j],'view_position'] == view_position) :
                    Prior_csv.loc[index[j],'Distance'] = Distance
                    Prior_csv.loc[index[j],'Hours'] = hours
                    Prior_csv.loc[index[j],'minutes'] = minutes
                Prior_csv.to_csv('a.csv')
        
        #print Distance
        #print hours
        #print minutes
        #if i == 
    '''
    a_csv = pd.read_csv('a.csv')
    for i in range(len(a['sop_instance_uid'])):
        sopuid = a.loc[i,'sop_instance_uid']
        dcm_filename    = Dir + '/Priors/' + '/DCMs/' + sopuid + '.dcm'
        png_filename    = Dir + '/Priors/' + '/outputs/' + sopuid +'._1_.png'
        nipple_filename = Dir + '/Priors/' + '/JSON-Outputs/' + sopuid + '_MassCalc.json'
        metadata_filename = Dir + '/Priors/' + '/JSON-Outputs/' + sopuid + '_MetaData.json'
        breast_contour_filename = Dir + '/Priors/' + '/BreastBoundary_Output/' + sopuid + '.json'
        cmfn = a.loc[i,'Distance']
        hours = a.loc[i,'Hours']
        minute = a.loc[i,'minutes']        
        clock_angle = str(hours) + ":" + str(minutes)
        Mark_and_Protract(clock_angle, dcm_filename, nipple_filename, png_filename)
        '''